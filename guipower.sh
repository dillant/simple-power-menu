#!/usr/bin/env bash

power=$(yad --list --title "power" --height=259 --width=816 \
    --column=Power \
    Shutdown Reboot Exit)

if [[ $power == "Shutdown|" ]]; then
    shutdown now
elif [[ $power == "Reboot|" ]]; then
    reboot
elif [[ $power == "Exit|" ]]; then
    herbstclient quit
fi
