#!/usr/bin/env bash


function main() {

    power=$(whiptail --title "POWERMENU" --menu "Would would you like to do?" 0 0 0 \
        "Reboot" "restart the computer" \
        "Shutdown" "power off computer" \
        "Exit" "Log out computer" \
        3>&1 1>&2 2>&3
        )

    if [[ $power == "Reboot" ]]; then
        reboot
    elif [[ $power == "Shutdown" ]]; then
        shutdown now
    elif [[ $power == "Exit" ]]; then
        herbstclient quit
    fi
}

main
